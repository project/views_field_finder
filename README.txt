
The "Views Field Finder" module

SUMMARY
=======
The purpose of this module is to enable site builders and developers to search and find where particular fields
are been used in Views.  This will come in handy when, for example, you want to see it how many times or in what
Views Displays a certain field is being used.

Installation and configuration
==============================
https://www.drupal.org/project/views_field_finder

LINKS
=====

For a full description, visit the project page
http://drupal.org/project/views_field_finder

Bug reports, feature suggestions, and latest developments:
http://drupal.org/project/issues/views_field_finder